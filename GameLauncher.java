import java.util.Scanner;

public class GameLauncher 
{
	/*
	- lets the user play chose a game to play
	- after a play throught method asks if user wants to play again.
	*/
	public static void main(String[] arg){
		
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		playGame();
		
		boolean playAgain = true;
		
		while (playAgain) {
			
		System.out.println("Would you like to play again?");
		System.out.println("Input a negative number to play again or input a positive number to stop playing. *(0 is considered a positive number)*");
		
		double userInput = reader.nextDouble();
		
			if (userInput < 0) {
				playGame();
			}
			else {
				playAgain = false;
			}
		}
	}	
		/*
		- asks user to chose a game to play and runs the game they chose to play
		- game choices are Hangman and Wordle
		*/
	public static void playGame() {
		
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		System.out.println("Would you like to play Wordle or Hangman?");
		System.out.println("Input a negative whole number to play Wordle or input a positive whole number to play Hangman. *(0 is considered a positive number)*");
		
		int userInput = reader.nextInt();
		
		if (userInput < 0) {
			String answer = Wordle.generateWord();
			Wordle.runGame(answer);
		}
		else {
			System.out.println("Enter a 4-letter word:");
			String word = reader.next();
			word = word.toUpperCase();
			Hangman.runGame(word);
		}
	}
}
			
		